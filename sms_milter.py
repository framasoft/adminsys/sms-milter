#!/usr/bin/env python3

"""Milter to spot very urgent mails and send SMS and Gotify messages about them"""

from __future__ import print_function
from pprint import pprint
import re
import sys
import time
from typing import Union

import envkey
from gotify_message import GotifyNotification
import sib_api_v3_sdk
from sib_api_v3_sdk.rest import ApiException
import Milter
import redis
import setproctitle

setproctitle.setproctitle("sms-milter")


abuse_id_reg = re.compile(r"(AbuseID:[^\]]*)")
csam_reg = re.compile(r"Child Sexual Abuse Material|CSAM")
ftalk_reg = re.compile(r"(https://framatalk.org/\w*)")

r = redis.Redis(
    host="localhost", port=6379, db=envkey.get("REDIS_DB_NB"), decode_responses=True
)


def send_messages(  # pylint: disable-msg=too-many-locals
        abuse_id: str, delay: Union[str, None], concerns: str, ftalk: Union[str, None], urgent: bool
) -> None:
    """Send warnings with SMS and Gotify"""
    gotify_url = envkey.get("GOTIFY_URL")

    title = f"{concerns} Attention ! Mail d’Abuse !"
    if urgent:
        title = f"{concerns} URGENCE ! Mail d’Abuse Urgent !"
    msg = f"Abuse ID : {abuse_id}"
    print(
        f"""{title}
{msg}"""
    )
    if delay is not None:
        msg += f"\n{delay}"
    if ftalk is not None:
        msg += f"\nÇa concerne Framatalk: {ftalk}"

    brevo_config = sib_api_v3_sdk.Configuration()
    brevo_config.api_key["api-key"] = envkey.get("BREVO_SMS_APIKEY")
    brevo_api = sib_api_v3_sdk.TransactionalSMSApi(
        sib_api_v3_sdk.ApiClient(brevo_config)
    )

    for tel_num in envkey.get("TEL_NUMS").split(" "):
        send_transac_sms = sib_api_v3_sdk.SendTransacSms(
            sender="Framalerte",
            recipient=str(tel_num),
            content=f"{title}\n{msg}",
            type="transactional",
            tag="pymilter",
            unicode_enabled=True,
            organisation_prefix="[Framasoft]",
        )
        try:
            api_response = brevo_api.send_transac_sms(send_transac_sms)
            pprint(api_response)

            # OMG, we’re running out of credits!
            if api_response.remaining_credits < 100:
                for token in envkey.get("GOTIFY_TOKENS").split(" "):
                    message = GotifyNotification(
                        url=gotify_url,
                        token=token,
                        title="Plus assez de crédits SMS Brevo",
                        message="Il ne reste plus que "
                        f"{api_response.remaining_credits} "
                        "crédits, il faut en racheter sur "
                        "https://app.brevo.com/billing/account/"
                        "customize/message-credits",
                        priority=9,
                    )
                    message.send()

        except ApiException as e:
            print(
                f"Exception when calling TransactionalSMSApi->send_transac_sms: {e}\n"
            )

    for token in envkey.get("GOTIFY_TOKENS").split(" "):
        message = GotifyNotification(
            url=gotify_url, token=token, title=title, message=msg, priority=9
        )
        message.send()


class UrgentMilter(Milter.Base):  # pylint: disable-msg=too-many-instance-attributes
    """Spot very urgent mails"""

    def __init__(self):  # A new instance with each new connection.
        self.id = Milter.uniqueID()  # Integer incremented with each call.
        self.abuse_id = None
        self.delay = None
        self.concerns = ""
        self.ftalk = None
        self.message_id = None
        self.already_seen = False
        self.urgent = False

    @Milter.noreply
    def header(self, field, value):
        # We need an unique identifier, so let’s get the Message-ID
        if field == "Message-ID" and str(value) != "None":
            self.message_id = value
            already_seen = r.hgetall(str(value))
            if not already_seen:
                r.hset(str(value), mapping={"time": time.time()})
            else:
                print(f"{value} has already been seen. Not doing anything")
                self.already_seen = True

            return Milter.CONTINUE

        # Let’s check the subject
        if field == "Subject":
            if "AbuseUrgent" in value or "AbuseNormal" in value:
                res = abuse_id_reg.search(value)
                if res is not None:
                    self.abuse_id = res.group(1)

                    if str(self.message_id) != "None":
                        record = r.hgetall(str(self.message_id))
                        record["abuse_id"] = self.abuse_id
                        r.hset(str(self.message_id), mapping=record)
            else:
                if str(self.message_id) != "None":
                    r.delete(str(self.message_id))

            return Milter.CONTINUE

        return Milter.CONTINUE

    # Let’s dig deeper
    @Milter.noreply
    def body(self, blk):
        if not self.already_seen:
            if (
                "next 1 (one!) hour" in str(blk)
                or "next hour" in str(blk)
                or "we will lock the IP" in str(blk)
            ):
                self.urgent = True
                self.delay = "On a 1h pour répondre !"
            if "next *24 hours*" in str(blk):
                self.delay = "On a 24h pour répondre."
            if "A statement has been successfully entered for this issue" in str(blk):
                self.already_seen = True
            if csam_reg.search(str(blk)):
                self.concerns = "[PED0P0RN]"
            res = ftalk_reg.search(str(blk))
            if res is not None:
                self.ftalk = res.group(1)

        return Milter.CONTINUE

    # When we reach the end of the mail, we can send a warning if needed
    def eom(self):
        if not self.already_seen:
            if self.abuse_id is not None:
                send_messages(
                    abuse_id=self.abuse_id,
                    delay=self.delay,
                    concerns=self.concerns,
                    ftalk=self.ftalk,
                    urgent=self.urgent
                )
        return Milter.CONTINUE


def main():
    """Start milter"""

    socketname = envkey.get("MILTER_SOCKET")
    timeout = 600
    # Register to have the Milter factory create instances of your class:
    Milter.factory = UrgentMilter
    print(f"{time.strftime('%Y-%m-%d %H:%M:%S')} milter startup")
    sys.stdout.flush()
    Milter.runmilter("urgent-milter-filter", socketname, timeout)
    print(f"{time.strftime('%Y-%m-%d %H:%M:%S')} urgent milter shutdown")


if __name__ == "__main__":
    main()
