# SMS Milter

This is a milter (mail filter) which checks the contents of incoming mails and warns us with SMS sent via [Brevo API](https://developers.brevo.com/reference/sendtransacsms) and [Gotify](https://gotify.net) messages.

## Installation

### Get the code

```bash
cd /opt
git clone https://framagit.org/framasoft/adminsys/sms-milter.git
sudo adduser --system --home /opt/sms-milter --no-create-home --shell /bin/false sms-milter
sudo chown -R sms-milter: /opt/sms-milter
```

### Dependencies

```bash
sudo apt install -y python3-dev \
                    python3-virtualenv \
                    libmilter-dev \
                    redis-server
```

```bash
cd /opt/sms-milter
virtualenv venv
. ./venv/bin/activate
```

```bash
pip install pymilter \
            setproctitle \
            sib_api_v3_sdk \
            "redis[hiredis]" \
            envkey \
            gotify_message
```

NB: we use an [Envkey](https://envkey.com/) server to store some secrets and configuration.

### Configuration

Modify the `.envkey` file to suit your Envkey server.

Get an `ENVKEY` token from the Envkey server and put it either in `~/.env` or in a `.env` file in the installation directory.

Here are the variables to create in your Envkey server:

- `REDIS_DB_NB`: number of the Redis database to use
- `GOTIFY_URL`: address of your Gotify server, with the scheme. Ex: `https://gotify.example.org`
- `GOTIFY_TOKENS`: a space-separated list of Gotify tokens to send messages to
- `BREVO_SMS_APIKEY`: the API key for Brevo. Get it on <https://app.brevo.com/settings/keys/api>
- `TEL_NUMS`: a space-separated list of telephone numbers to send SMS to, with the international dialling code. Ex: `33199004242`
- `MILTER_SOCKET`: the socket where the milter will listen. It can be a unix socket (Ex: `unix:/var/run/sms-milter`) or a network one (Ex: `inet:7871@localhost`). See [milter.setconn](https://pymilter.org/pymilter/namespacemilter.html#a47dedd3e5972a865bcfaa5163eb481cb) documentation for more information

### Service

```bash
cp sms-milter.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now sms-milter.service
```

## Make Postfix using it

Add the milter to the `smtpd_milters` setting in `/etc/postfix/main.cf`.

Exemple, if you’re using `127.0.0.1:8020` for the socket:
```ini
smtpd_milters = inet:127.0.0.1:8020
```

If you’re using multiple milters, separate them with a space:
```ini
smtpd_milters = inet:127.0.0.1:2500 inet:localhost:11332 inet:localhost:8020
```

## License

This software is licensed under the terms of the GNU GPLv3. See [LICENSE](LICENSE) file.
